//Global jquery store for button position
var $currentButton;
var $currentButtonRight;
//cancel
$('body').on('click', '.button-left', function(){
    $(".modalCenterWebInputUpload").val("");
    $(".modalCenterWebInputDescription").val("");
    $currentButton = $(this);
});
$('body').on('click', '.button-right',function(){
    $(".ModalCenterFolderInputUpload").val("");
    $(".ModalCenterFolderInputDescription").val("");
    $('.modalCenterFolderInputUploadshow').val("");
    $currentButtonRight = $(this);
});
//Global save modal for WEB
//Description input 
var savemodal = function (){
   var take = $('.modalCenterWebInputDescription').val();
   $currentButton.prev('p').text(take);
    $currentButton.addClass('add-class-color');
    $currentButtonRight.removeClass('add-class-color');
}
//Description input 
$('.modalCenterWebInputDescription').change(function () {
    savemodal();
    }); 
//Upload Data-Attribute
var dataModalCenterWebInputUpload = function(){
    var take = $('.modalCenterWebInputUpload').val();
    $currentButton.closest('.awards').data('saved',take);
}
$('body').on('change','.modalCenterWebInputUpload', function () {
    dataModalCenterWebInputUpload();
    }); 
//WEB pattern youtube
var check = function(ischecked){
    var chechtube = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
    return chechtube.test(ischecked);
}
var validateImageModal = function(){
    var validateModalCenter = $(".modalCenterWebInputUpload").val();
    if(check(validateModalCenter)){
        $currentButton.closest('.awards').find('img').first().addClass('ytvideo').removeClass('web');
    }else{
        $currentButton.closest('.awards').find('img').first().addClass('web').removeClass('ytvideo');
    }
}
//Url Mp4 image check pattern and Mp3
var validateVideoOnline = function(){
    var check = $(".modalCenterWebInputUpload").val();
    var ischeckedvideo = check.match(/(.flv|.wmv|.mov|.mp4|.avi)/);
    var ischeckedradio = check.match( /(.wav|.mp3|.aac)/);
    if(ischeckedvideo){
     $currentButton.closest('.awards').find('img').first().addClass('ytvideo').removeClass('web');
    }else if(ischeckedradio){
        $currentButton.closest('.awards').find('img').first().addClass('video').removeClass('web');
    };
}
$('body').on('focusout','.modalCenterWebInputUpload',function(){
    validateImageModal();
    validateVideoOnline();
    $currentButton.addClass('add-class-color');
    $currentButtonRight.removeClass('add-class-color');
})
//Global save modal for FOLDER
//Description input 
$('body').on('change', '.ModalCenterFolderInputDescription', function () {
    saveModalFolderDescription();
    });

var saveModalFolderDescription = function (){
   var take = $('.ModalCenterFolderInputDescription').val();
   $currentButtonRight.prev().prev().text(take);
    $currentButtonRight.addClass('add-class-color');
    $currentButton.removeClass('add-class-color');
}
//Upload Data-attribute
var dataModalCenterFolderInputUpload = function(){
    var take = $('.modalCenterFolderInputUpload').val();
    take = take.replace('C:\\fakepath\\', ''); 
    $currentButtonRight.closest('.awards').data('saved',take);
}

//Global check for Folder Image Pattern
var folderchecked = function(){
    var check = $(".ModalCenterFolderInputUpload").val();
    var ischeckedradio = check.match( /(.wav|.mp3|.aac)/);
    var ischeckedtv = check.match(/(.flv|.wmv|.mov|.mp4|.avi)/);

    if(ischeckedradio){
         $currentButtonRight.closest('.awards').find('img').first().addClass('audio').removeClass('video').removeClass('application');
    }else if(ischeckedtv){
        $currentButtonRight.closest('.awards').find('img').first().addClass('video').removeClass('application').removeClass('audio');
    }else{
        $currentButtonRight.closest('.awards').find('img').first().addClass('application').removeClass('audio').removeClass('video'); 
    }
}
 
$('body').on('change','.ModalCenterFolderInputUpload',function () {
        folderchecked();
        var take = $(this).val();
        take = take.replace('C:\\fakepath\\', ''); 
        $('.modalCenterFolderInputUploadshow').val(take);
}); 

