generale();

function generale (){
    if($('body').is('.body-userDetails')){
            userdetails();
      }
    else if($('body').is('.body-awards')){
            awards();
      } 
    else if($('body').is('.body-userAdd')){
        userAdd();
        uploadUserImage();
      } 
};

//
function userdetails(){
    $('.userDetails-hands').on('click', function(){
        if( $('.showUser-sx').hasClass('hidden')){
            $('.showUser-dx').addClass('hidden')
            $('.showUser-sx').removeClass('hidden')	
    }else{
         $('.showUser-dx').removeClass('hidden')
         $('.showUser-sx').addClass('hidden')
    }
        
    })

    $('.child-userDetailsLevel').on('click',function(){
         $(this).find('input').focus();
    })


    $(document).ready(function(){
     $('#datepicker').datepicker(); 
    })
     $('.animal').on('click',function(){
             var src = $(this).find('img').attr('src');
            $('#profiles').attr('src', src);
            $('.userDetailsEdit-child').removeClass('removeForWeb');
            $('#myCanvas').addClass('removeForWeb');
        })
    
    // start web
navigator.getUserMedia = ( navigator.getUserMedia ||
                             navigator.webkitGetUserMedia ||
                             navigator.mozGetUserMedia ||
                             navigator.msGetUserMedia);

var $video = $('video')[0];
var webcamStream;

function startWebcam() {
    if (navigator.getUserMedia) {
            navigator.getUserMedia (
        {
            video: true,
            audio: false
        },
        function(localMediaStream) {
            $video.src = window.URL.createObjectURL(localMediaStream);
            webcamStream = localMediaStream;
        },
                
        function(err) {
            console.log("The following error occured: " + err);
        });
    }  
}
var $canvas, ctx;

function init() {
    $canvas = $("#myCanvas")[0];
    ctx = $canvas.getContext('2d');
}

function snapshot() {
    ctx.drawImage(video, 0,0, $canvas.width, $canvas.height);
}

$( window ).on('load', function() {
   init();
});

$(video).on('click',function(){
    snapshot(this);
}) 
    $('.userDetails-edit').on('click',function(){
    startWebcam();
})
   $('.takeShot').on('click', function(){
       snapshot();
    $('.userDetailsEdit-child').addClass('removeForWeb');
    $('#myCanvas').removeClass('removeForWeb');
    $('#myCanvas').removeClass('x'); $('#myCanvas').addClass('hideCanvas').addClass('canvasTake');
      
       
})

}
