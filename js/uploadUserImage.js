
function uploadUserImage (){ 
    
    
    $("#showImageUser").change(function(){
        readURL(this);
    });
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
//

var validate = function(e){
    var check = /^$|\s+/;
    return check.test(e);
}
var vrf = function(){
    var fieldemptyName = $('.nameUser').val();
    var fieldemptyCogn = $('.cognomeUser').val();
    var fieldemptyData =  $('.dataUser').val();
    if (validate(fieldemptyName)){
        $('.nameUser').addClass('error-field-empty');
        $('.labelNome').addClass('error-field-label');

    }else if (fieldemptyName !== "" ){
         $('.nameUser').removeClass('error-field-empty');
         $('.labelNome').removeClass('error-field-label');

              };
    if(validate(fieldemptyCogn)){
        $('.cognomeUser').addClass('error-field-empty');
        $('.labelCognome').addClass('error-field-label');
    }else if (fieldemptyCogn !== "" ){
        $('.cognomeUser').removeClass('error-field-empty');
        $('.labelCognome').removeClass('error-field-label');
              };
    if(validate(fieldemptyData)){
        $('.dataUser').addClass('error-field-empty');
        
       }else if (fieldemptyData !== "" ){
         $('.dataUser').removeClass('error-field-empty');
              };;
}

$('.button-save-user').on('click', function(){
    vrf();
    $(".validateDataUser").click();

})
//

//
$('.button-close-user').on('click', function(){
    $('.nameUser').val('');
    $('.cognomeUser').val('');
    $('.dataUser').val('');
    var src = "../img/cat.png";
    $('.randomDefaultUserBig').attr('src',src);
    $('.randomDefaultUserBigS').attr('src',src);
    $('.nameUser').removeClass('error-field-empty');
    $('.cognomeUser').removeClass('error-field-empty');
    $('.dataUser').removeClass('error-field-empty');
    $('#myCanvas').addClass('x');
    $('.randomDefaultUserBig').removeClass('removeForWeb');
    $('.labelCognome').removeClass('error-field-label');
    $('.labelNome').removeClass('error-field-label');
})
//
$('.animal').on('click',function(){
     var src = $(this).find('img').attr('src');
    $('#profile').attr('src', src);
    $('.randomDefaultUserBig').removeClass('removeForWeb');
    $('#myCanvas').addClass('removeForWeb');
})

$('#showImageUser').on('click', function(){
    $('.randomDefaultUserBig').removeClass('removeForWeb'); 
    $('#myCanvas').addClass('removeForWeb');
})
 $(document).ready(function(){
     $('#datepickerAdd').datepicker(); 
    })
}
//
