function awards (){
    var template = $('#awards').html();
    var templateScript = Handlebars.compile(template);

    $.ajax({
        url: "prizes.json",
    })
    .done(function(result){
        var html = templateScript(result);
        $('.row-start-wall').append(html);
    });
    };

