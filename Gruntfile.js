module.exports = function(grunt) {
  
  // Load automatically all tasks without using grunt.loadNpmTasks()
  // for each module
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    
    /*
     * LESS task configuration
     */
    less: {
      dist: {
        files: {
          'css/main.css': [ // <== Write here the path to the compiled CSS
            'less/main.less' // <== Write here the path to the LESS file(s)
          ]
        },
        options: {
          compress: true, // This option minimizes the CSS
            sourceMap: true,
        sourceMapName: 'css/main.css.map'
        }
      }
    },
    
    /*
     * Watch task configuration
     */
    watch: {
        
      less: {
        files: [
          'less/main.less',
            'less/userAdd.less',
            'less/nav.less',
            'less/variables',
            'less/searchUser.less',
            'less/userDetails.less'
            
        ],
        tasks: ['less']
      },
        js:{
            files:[
                'js/transfer-val.js',
                'js/ajaxTemplateAwards.js',
                'js/webcamUser.js',
                'js/uploadUserImage.js',
                'js/userDetails.js'
            ],
            tasks:['uglify']
        }
    },
 uglify: {
    my_target: {
        options: {
        sourceMap: true,
        sourceMapName: 'output.min.js.map'
      },
      files: {
        'output.min.js': ['node_modules/jquery/dist/jquery.min.js','node_modules/bootstrap/dist/js/bootstrap.min.js','node_modules/handlebars/dist/handlebars.min.js','js/userDetails.js','node_modules/bootstrap-datepicker/js/bootstrap-datepicker.js','js/transfer-val.js', 'js/ajaxTemplateAwards.js','js/webcamUser.js', 'js/uploadUserImage.js' ]
          
      }
    }
  }
  });


  /*
   * Registered tasks
   */
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['less','uglify','watch']);


};